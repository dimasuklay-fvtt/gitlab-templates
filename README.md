# Gitlab Templates

CI/CD templates for building and releasing [Foundry VTT](https://foundryvtt.com/) packages.

## Usage

1. In your `.gitlab-ci.yml`, add the following code:

      ```
      include:
        - https://gitlab.com/api/v4/projects/28119040/packages/generic/gitlab-templates/latest/foundry-publish.yml
      ```

      Or, if you want to use the raw (possibly untested and unreleased) version:

      ```
      include:
        - https://gitlab.com/dimasuklay-fvtt/gitlab-templates/-/raw/main/foundry-publish.yml
      ```

1. Set the foundryvtt.com variables `FVTT_USERNAME`, `FVTT_PASSWORD`, and `FVTT_PACKAGE_ID` in your project's Settings > CI/CD > Variables. When you are done, it should look like this:

      ![Screenshot of CI/CD variables](docs/gitlab_variables.jpg)

1. Set the remaining environment variables `FVTT_MANIFEST_URL` and `FVTT_MANIFEST_FILE` in your `.gitlab-ci.yml`. Since the correct values depend on the output of your build process, the actual values are up to you. `FVTT_MANIFEST_URL` should be a URL specific to the version you are releasing, not your stable "latest" URL. `FVTT_MANIFEST_FILE` should be present in the job's local file system.

1. Create a new job that extends the pipeline. At the bare minimum, you need to define the variables mentioned in the previous steps and set the `stage` where the job runs.

      ```
      publish:
        extends: .foundry-publish
        stage: publish
        variables:
          FVTT_MANIFEST_URL: https://your.manifest/url
          FVTT_MANIFEST_FILE: /path/to/your/manifest
      ```

## Live Examples

* [Sandbox Explorer](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/-/blob/main/.gitlab-ci.yml#L75)
