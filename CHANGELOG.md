# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.0.0 (2021-07-17)


### Features

* initial release ([deec90f](https://gitlab.com/dimasuklay-fvtt/gitlab-templates/commit/deec90fa570d1572d007e913fc11e7d7e47536b7))
